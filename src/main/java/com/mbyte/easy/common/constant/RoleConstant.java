package com.mbyte.easy.common.constant;

import com.mbyte.easy.util.Utility;

/**
 * 用户角色常量
 * @author zte
 * @since 2020/10/5 23:25
 */
public class RoleConstant {

    /**
     * rest接口请求前缀
     */
    public final static String API_URI_PREFIX = "/rest";


    /**
     * 角色ID：
     */
    // 管理员
    public static final Long ROLE_ID_ADMIN = 1L;
    // 学生
    public static final Long ROLE_ID_STUDENT = 2L;


}
