package com.mbyte.easy.rest.login;

import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.security.entity.SysUser;
import com.mbyte.easy.security.service.IUserService;
import com.mbyte.easy.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author zte
 * @since 2020/12/10 23:36
 */
@RestController
@RequestMapping("rest/login")
public class RestLogin extends BaseController {

    @Autowired
    private IUserService userService;

    @Resource
    private CacheManager cacheManager;

    @GetMapping("/login")
    @ResponseBody
    public AjaxResult login(@RequestParam("stuNumber") String stuNumber
            , @RequestParam("password")String password){

        Map<String , Object> map = new HashMap<>();
        SysUser user = userService.selectByUsername(stuNumber);
        if(user != null && Utility.checkPassword(password , user.getPassword())){
            Cache cache = cacheManager.getCache("serviceCacheToken");
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            //放入缓存
            cache.put(uuid, user);
            map.put("uuid",uuid);
            map.put("msg","success");
            return success(map);
        }else {
            return error("用户名或密码不正确");
        }
    }

}
