package com.mbyte.easy.rest.classInfo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.service.IClassInfoService;
import com.mbyte.easy.admin.service.IStudentService;
import com.mbyte.easy.common.constant.RoleConstant;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.security.entity.SysRole;
import com.mbyte.easy.security.entity.SysUser;
import com.mbyte.easy.security.service.IRoleService;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/classInfo")
public class RestClassInfoController extends BaseController  {

    @Autowired
    private IClassInfoService classInfoService;
    @Resource
    private CacheManager cacheManager;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IStudentService studentService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo
            ,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize
            ,@RequestParam("token") String token) {
        Cache cache = cacheManager.getCache("serviceCacheToken");
        SysUser user = cache.get(token, SysUser.class);
        if(user != null){
            Map<String, Object> map = new HashMap<>();
            SysRole sysUserRole = null;
            List<SysRole> sysUserRoles = roleService.selectRolesByUserID(user.getId());
            if(!CollectionUtils.isEmpty(sysUserRoles)){
                sysUserRole = sysUserRoles.get(0);
            }
            Page<ClassInfo> page = new Page<ClassInfo>(pageNo, pageSize);
            QueryWrapper<ClassInfo> queryWrapper = new QueryWrapper<ClassInfo>();
            if(sysUserRole != null){
                if(sysUserRole.getId().equals(RoleConstant.ROLE_ID_STUDENT)){
                    Student student = studentService.getOne(new LambdaQueryWrapper<Student>()
                            .eq(Student::getStuNumber, user.getUsername()));
                    queryWrapper.eq("id",student.getClassInfoId());
                }
            }
            IPage<ClassInfo> pageInfo = classInfoService.page(page, queryWrapper);
            map.put("pageInfo", pageInfo.getRecords());
            return this.success(map);
        }else {
            return error();
        }
    }


    /**
    * 添加
    * @param classInfo
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(ClassInfo classInfo){
        return toAjax(classInfoService.save(classInfo));
    }

    /**
    * 添加
    * @param classInfo
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(ClassInfo classInfo){
        return toAjax(classInfoService.updateById(classInfo));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete")
    public AjaxResult delete(@RequestParam("id") Long id){
        return toAjax(classInfoService.removeById(id));
    }

    /**
     * 根据班级ID找到对应的学生名单
     * @param id
     * @return
     */
    @GetMapping("getStudent")
    @ResponseBody
    public AjaxResult getStudent(@RequestParam("id") Long id){
        return success(classInfoService.getStud(id));
    }

}

