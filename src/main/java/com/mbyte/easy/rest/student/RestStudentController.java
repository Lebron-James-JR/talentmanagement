package com.mbyte.easy.rest.student;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.service.IStudentCourseService;
import com.mbyte.easy.admin.service.IStudentService;
import com.mbyte.easy.common.constant.RoleConstant;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.security.entity.SysRole;
import com.mbyte.easy.security.entity.SysUser;
import com.mbyte.easy.security.entity.SysUserRoles;
import com.mbyte.easy.security.service.IRoleService;
import com.mbyte.easy.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/student")
public class RestStudentController extends BaseController  {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private IStudentCourseService studentCourseService;
    @Resource
    private CacheManager cacheManager;
    @Autowired
    private IRoleService roleService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo
            ,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize
            ,@RequestParam(value = "token") String token) {
        Cache cache = cacheManager.getCache("serviceCacheToken");
        SysUser user = cache.get(token, SysUser.class);
        if(user != null){
            SysRole sysUserRole = null;
            List<SysRole> sysUserRoles = roleService.selectRolesByUserID(user.getId());
            if(!CollectionUtils.isEmpty(sysUserRoles)){
                sysUserRole = sysUserRoles.get(0);
            }
            Page<Student> page = new Page<Student>(pageNo, pageSize);
            QueryWrapper<Student> queryWrapper = new QueryWrapper<Student>();
            Map<String, Object> map = new HashMap<>();
            if(sysUserRole != null){
                if(sysUserRole.getId().equals(RoleConstant.ROLE_ID_STUDENT)){
                    queryWrapper.eq("stu_number",user.getUsername());
                }
            }

            IPage<Student> pageInfo = studentService.getPage(page, queryWrapper);
            map.put("pageInfo", pageInfo.getRecords());
            return this.success(map);
        }else {
            return error();
        }

    }


    /**
    * 添加学生信息
    * @param student
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(Student student){
        return toAjax(studentService.saveStu(student));
    }

    /**
     * 编辑学生信息
     * @param stuName
     * @param id
     * @param sex
     * @param age
     * @param tel
     * @return
     */
    @PostMapping("edit")
    public AjaxResult edit(@RequestParam("stuName") String stuName
            , @RequestParam("id") Long id
            , @RequestParam("sex") Integer sex
            , @RequestParam(value = "age",required = false) Integer age
            , @RequestParam(value = "tel",required = false)String tel){
        UpdateWrapper<Student> wrapper = new UpdateWrapper<>();
        wrapper.eq("id" , id);
        wrapper.set("stu_name",stuName);
        wrapper.set("sex",sex);
        if(age != null){
            wrapper.set("age",age);
        }
        if(tel != null){
            wrapper.set("tel",tel);
        }
        return toAjax(studentService.update(wrapper));
    }

    @GetMapping("delete")
    @ResponseBody
    public AjaxResult delete(@RequestParam("id") Long id){
        return toAjax(studentService.deletById(id));
    }

    /**
     * 根据主键ID得到学生信息
     * @param id
     * @return
     */
    @GetMapping("getStuById")
    @ResponseBody
    public AjaxResult getStuInfo(@RequestParam("id") Long id){
        return success(studentService.getStuById(id));
    }

    /**
     * 根据学生ID获取学生对应课程列表
     * @param sid
     * @return
     */
    @GetMapping("getCourseStu")
    @ResponseBody
    public AjaxResult getCourse(@RequestParam("sid")Long sid){
        return success(studentCourseService.getCourseStu(sid));
    }

}

