package com.mbyte.easy.rest.studentCourse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.StudentCourse;
import com.mbyte.easy.admin.service.IStudentCourseService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@RestController
@RequestMapping("rest/studentCourse")
public class RestStudentCourseController extends BaseController  {

    @Autowired
    private IStudentCourseService studentCourseService;

    /**
    * 查询列表
    *
    * @param pageNo
    * @param pageSize
    * @param studentCourse
    * @return
    */
    @RequestMapping
    public AjaxResult index(@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createTimeSpace, String deleteTimeSpace, StudentCourse studentCourse) {
        Page<StudentCourse> page = new Page<StudentCourse>(pageNo, pageSize);
        QueryWrapper<StudentCourse> queryWrapper = new QueryWrapper<StudentCourse>();

        if(studentCourse.getStuId() != null  && !"".equals(studentCourse.getStuId() + "")) {
            queryWrapper = queryWrapper.like("stu_id",studentCourse.getStuId());
         }


        if(studentCourse.getCourseId() != null  && !"".equals(studentCourse.getCourseId() + "")) {
            queryWrapper = queryWrapper.like("course_id",studentCourse.getCourseId());
         }


        if(studentCourse.getScore() != null  && !"".equals(studentCourse.getScore() + "")) {
            queryWrapper = queryWrapper.like("score",studentCourse.getScore());
         }


        IPage<StudentCourse> pageInfo = studentCourseService.page(page, queryWrapper);

        Map<String, Object> map = new HashMap<>();
        map.put("searchInfo",  studentCourse);
        map.put("pageInfo", new PageInfo(pageInfo));

        return this.success(map);
    }


    /**
    * 添加
    * @param studentCourse
    * @return
    */
    @PostMapping("add")
    public AjaxResult add(StudentCourse studentCourse){
        return toAjax(studentCourseService.save(studentCourse));
    }

    /**
    * 添加
    * @param studentCourse
    * @return
    */
    @PostMapping("edit")
    public AjaxResult edit(StudentCourse studentCourse){
        return toAjax(studentCourseService.updateById(studentCourse));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(studentCourseService.removeById(id));
    }

    @GetMapping("getStudent")
    @ResponseBody
    public AjaxResult getStudent(@RequestParam("cid") Long cid){
        List<StudentCourse> list = studentCourseService.getStuList(cid);
        return success(list);
    }

}

