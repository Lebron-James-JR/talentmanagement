package com.mbyte.easy.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mbyte.easy.admin.entity.Student;

import java.util.List;

/**
 * <p>
 * 班级信息表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface IClassInfoService extends IService<ClassInfo> {

    IPage<ClassInfo> getPage(Page<ClassInfo> page);


    List<Student> getStud(Long id);
}
