package com.mbyte.easy.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.mapper.StudentMapper;
import com.mbyte.easy.admin.service.IClassInfoService;
import com.mbyte.easy.admin.service.IStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 学生信息表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Transactional
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    @Autowired
    private IClassInfoService classInfoService;

    @Override
    public IPage<Student> getPage(Page<Student> ipage, QueryWrapper<Student> queryWrapper) {
        IPage<Student> page = page(ipage , queryWrapper);
        List<ClassInfo> classInfoList = classInfoService.list();
        Map<Long, String> classInfoListMap =
                classInfoList.stream().collect(Collectors.toMap(ClassInfo::getId , ClassInfo::getCourseName));
        List<Student> records = page.getRecords();
        for (Student record : records) {
            record.setClassName(classInfoListMap.get(record.getClassInfoId()));
        }
        return page;
    }

    @Override
    public int saveStu(Student student) {
        return this.getBaseMapper().saveStu(student);
    }

    @Override
    public int updateStu(Student student) {
        return this.getBaseMapper().updateStu(student);
    }

    @Override
    public int deletById(Long id) {

        return this.getBaseMapper().deleteStuById(id);
    }

    @Override
    public Student getStuById(Long id) {
        return this.getBaseMapper().getStuById(id);
    }
}
