package com.mbyte.easy.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Course;
import com.mbyte.easy.admin.entity.StudentCourse;
import com.mbyte.easy.admin.mapper.CourseMapper;
import com.mbyte.easy.admin.service.ICourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mbyte.easy.admin.service.IStudentCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程信息表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Service
@Slf4j
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Autowired
    private ICourseService courseService;
    @Autowired
    private IStudentCourseService studentCourseService;

    @Override
    public List<Course> getList(Long sid) {
        List<Course> courseList = courseService.list();
        List<Course> returnList = new ArrayList<>();

        for (Course course : courseList) {
            List<StudentCourse> list = studentCourseService.list(new LambdaQueryWrapper<StudentCourse>().eq(StudentCourse::getCourseId, course.getId()).eq(StudentCourse::getStuId, sid));
            if(list.size() == 0){
                returnList.add(course);
            }
        }
        return returnList;
    }

}
