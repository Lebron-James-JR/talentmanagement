package com.mbyte.easy.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程信息表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface ICourseService extends IService<Course> {

    /**
     * 得到课程列表
     * @return
     */
    List<Course> getList(Long sid);

}
