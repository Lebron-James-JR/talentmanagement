package com.mbyte.easy.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.mapper.ClassInfoMapper;
import com.mbyte.easy.admin.mapper.StudentMapper;
import com.mbyte.easy.admin.service.IClassInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 班级信息表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Service
public class ClassInfoServiceImpl extends ServiceImpl<ClassInfoMapper, ClassInfo> implements IClassInfoService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public IPage<ClassInfo> getPage(Page<ClassInfo> ipage) {
        return this.getBaseMapper().selectAll(ipage);
    }

    @Override
    public List<Student> getStud(Long id) {
        return studentMapper.getStu(id);
    }
}
