package com.mbyte.easy.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.entity.StudentCourse;
import com.mbyte.easy.admin.mapper.StudentCourseMapper;
import com.mbyte.easy.admin.service.IClassInfoService;
import com.mbyte.easy.admin.service.IStudentCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mbyte.easy.admin.service.IStudentService;
import com.mbyte.easy.admin.vo.CourseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 学生--课程关联表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Service
public class StudentCourseServiceImpl extends ServiceImpl<StudentCourseMapper, StudentCourse> implements IStudentCourseService {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private IClassInfoService classInfoService;

    @Override
    public IPage<StudentCourse> getPage(Page<StudentCourse> ipage, QueryWrapper<StudentCourse> queryWrapper) {
        IPage<StudentCourse> page = page(ipage , queryWrapper);
        List<StudentCourse> records = page.getRecords();
        for (StudentCourse record : records) {
            Long stuId = record.getStuId();
            if(stuId != null){
                Student student = studentService.getById(stuId);
                if(student != null){
                    record.setStudentName(student.getStuName());
                    Long classInfoId = student.getClassInfoId();
                    if(classInfoId != null){
                        ClassInfo classInfo = classInfoService.getById(classInfoId);
                        if(classInfo != null){
                            record.setClassName(classInfo.getCourseName());
                        }
                    }
                    if(student.getSex() != null){
                        if(student.getSex() == 0){
                            record.setSex("男");
                        }
                        if(student.getSex() == 1){
                            record.setSex("女");
                        }
                    }
                }
            }
        }
        return page;
    }

    @Override
    public List<CourseVo> getCourseStu(Long sid) {
        return this.getBaseMapper().getCourseStu(sid);
    }

    @Override
    public List<StudentCourse> getStuList(Long cid) {
        return this.getBaseMapper().getStuList(cid);
    }
}
