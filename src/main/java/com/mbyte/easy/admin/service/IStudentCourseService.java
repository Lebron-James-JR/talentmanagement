package com.mbyte.easy.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.StudentCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mbyte.easy.admin.vo.CourseVo;

import java.util.List;

/**
 * <p>
 * 学生--课程关联表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface IStudentCourseService extends IService<StudentCourse> {

    IPage<StudentCourse> getPage(Page<StudentCourse> page, QueryWrapper<StudentCourse> queryWrapper);

    /**
     * 得到学生对应的课程列表
     * @param sid
     * @return
     */
    List<CourseVo> getCourseStu(Long sid);

    /**
     * 得到指定课程下的学生列表以及分数
     * @param cid
     * @return
     */
    List<StudentCourse> getStuList(Long cid);

}
