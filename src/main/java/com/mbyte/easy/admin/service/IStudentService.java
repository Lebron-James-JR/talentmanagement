package com.mbyte.easy.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学生信息表 服务类
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface IStudentService extends IService<Student> {

    IPage<Student> getPage(Page<Student> page, QueryWrapper<Student> queryWrapper);

    /**
     * 添加一条学生记录
     * @param student
     * @return
     */
    int saveStu(Student student);

    /**
     * 更新学生信息
     * @param student
     * @return
     */
    int updateStu(Student student);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    int deletById(Long id);

    /**
     * 得到学生信息
     * @param id
     * @return
     */
    Student getStuById(Long id);
}
