package com.mbyte.easy.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Course;
import com.mbyte.easy.admin.service.ICourseService;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.util.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/admin/course")
public class CourseController extends BaseController  {

    private String prefix = "admin/course/";

    @Autowired
    private ICourseService courseService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param course
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createTimeSpace, String deleteTimeSpace, Course course) {
        Page<Course> page = new Page<Course>(pageNo, pageSize);
        QueryWrapper<Course> queryWrapper = new QueryWrapper<Course>();
        if(!ObjectUtils.isEmpty(course.getCourseName())) {
            queryWrapper = queryWrapper.like("course_name",course.getCourseName());
         }
        if(!ObjectUtils.isEmpty(course.getRemark())) {
            queryWrapper = queryWrapper.like("remark",course.getRemark());
         }

        IPage<Course> pageInfo = courseService.page(page, queryWrapper);
        model.addAttribute("deleteTimeSpace", deleteTimeSpace);
        model.addAttribute("searchInfo", course);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(){
        return prefix+"add";
    }
    /**
    * 添加
    * @param course
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Course course){
        return toAjax(courseService.save(course));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        model.addAttribute("course",courseService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param course
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Course course){
        return toAjax(courseService.updateById(course));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(courseService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(courseService.removeByIds(ids));
    }

}

