package com.mbyte.easy.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.mbyte.easy.admin.entity.Student;
import com.mbyte.easy.admin.entity.StudentCourse;
import com.mbyte.easy.admin.service.IClassInfoService;
import com.mbyte.easy.admin.service.IStudentCourseService;
import com.mbyte.easy.admin.service.IStudentService;
import com.mbyte.easy.common.constant.RoleConstant;
import com.mbyte.easy.common.controller.BaseController;
import com.mbyte.easy.common.web.AjaxResult;
import com.mbyte.easy.security.entity.SysUser;
import com.mbyte.easy.security.entity.SysUserRoles;
import com.mbyte.easy.security.service.IUserService;
import com.mbyte.easy.util.PageInfo;
import com.mbyte.easy.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* <p>
* 前端控制器
* </p>
* @author lp
* @since 2019-03-11
*/
@Controller
@RequestMapping("/admin/student")
public class StudentController extends BaseController  {

    private String prefix = "admin/student/";

    @Autowired
    private IStudentService studentService;
    @Autowired
    private IClassInfoService classInfoService;
    @Autowired
    private IUserService userService;

    /**
    * 查询列表
    *
    * @param model
    * @param pageNo
    * @param pageSize
    * @param student
    * @return
    */
    @RequestMapping
    public String index(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, String createTimeSpace, String deleteTimeSpace, Student student) {
        Page<Student> page = new Page<Student>(pageNo, pageSize);
        QueryWrapper<Student> queryWrapper = new QueryWrapper<Student>();
        if(!ObjectUtils.isEmpty(student.getStuName())) {
            queryWrapper = queryWrapper.like("stu_name",student.getStuName());
         }
        if(!ObjectUtils.isEmpty(student.getClassInfoId())) {
            queryWrapper = queryWrapper.like("class_info_id",student.getClassInfoId());
         }
        if(!ObjectUtils.isEmpty(student.getAge())) {
            queryWrapper = queryWrapper.like("age",student.getAge());
         }
        if(!ObjectUtils.isEmpty(student.getTel())) {
            queryWrapper = queryWrapper.like("tel",student.getTel());
         }

        List<ClassInfo> list = classInfoService.list();
        model.addAttribute("classInfoList",list);
        IPage<Student> pageInfo = studentService.getPage(page, queryWrapper);
        model.addAttribute("deleteTimeSpace", deleteTimeSpace);
        model.addAttribute("searchInfo", student);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list";
    }

    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("addBefore")
    public String addBefore(Model model){
        List<ClassInfo> list = classInfoService.list();
        model.addAttribute("classInfoList",list);
        return prefix+"add";
    }
    /**
    * 添加
    * @param student
    * @return
    */
    @PostMapping("add")
    @ResponseBody
    public AjaxResult add(Student student){
        //用户表添加一条记录
        SysUser dbUser = userService.selectByUsername(student.getStuNumber());
        // 用户名已存在
        if (dbUser != null) {
            return error();
        }
        SysUser user = new SysUser();
        user.setUsername(student.getStuNumber());
        user.setPassword(Utility.QuickPassword(student.getStuNumber()));
        user.setCreatetime(new Date());
        user.setUpdatetime(new Date());
        userService.insert(user);
        user = userService.selectByUsername(student.getStuNumber());
        //添加角色
        SysUserRoles sysUserRoles = new SysUserRoles();
        sysUserRoles.setRolesId(RoleConstant.ROLE_ID_STUDENT);
        sysUserRoles.setSysUserId(user.getId());
        userService.insertuserRoles(sysUserRoles);

        return toAjax(studentService.save(student));
    }
    /**
    * 添加跳转页面
    * @return
    */
    @GetMapping("editBefore/{id}")
    public String editBefore(Model model,@PathVariable("id")Long id){
        List<ClassInfo> list = classInfoService.list();
        model.addAttribute("classInfoList",list);
        model.addAttribute("student",studentService.getById(id));
        return prefix+"edit";
    }
    /**
    * 添加
    * @param student
    * @return
    */
    @PostMapping("edit")
    @ResponseBody
    public AjaxResult edit(Student student){
        return toAjax(studentService.updateById(student));
    }
    /**
    * 删除
    * @param id
    * @return
    */
    @GetMapping("delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") Long id){
        return toAjax(studentService.removeById(id));
    }
    /**
    * 批量删除
    * @param ids
    * @return
    */
    @PostMapping("deleteAll")
    @ResponseBody
    public AjaxResult deleteAll(@RequestBody List<Long> ids){
        return toAjax(studentService.removeByIds(ids));
    }

    /**
     * 查询列表
     *
     * @param model
     * @param pageNo
     * @param pageSize
     * @param student
     * @return
     */
    @GetMapping("selectForClass/{clsId}")
    public String selectForClass(Model model,@RequestParam(value = "pageNo", required = false, defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize", required = false, defaultValue = "20") Integer pageSize, @PathVariable("clsId")Long clsId,  Student student) {
        Page<Student> page = new Page<Student>(pageNo, pageSize);
        QueryWrapper<Student> queryWrapper = new QueryWrapper<Student>();
        queryWrapper.eq("class_info_id",clsId);
        IPage<Student> pageInfo = studentService.getPage(page, queryWrapper);
        model.addAttribute("searchInfo", student);
        model.addAttribute("pageInfo", new PageInfo(pageInfo));
        return prefix+"list-class";
    }

}

