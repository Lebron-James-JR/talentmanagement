package com.mbyte.easy.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生信息表
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_student")
public class Student extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 学生姓名
     */
    private String stuName;

    /**
     * 学生学号
     */
    private String stuNumber;

    /**
     * 学生性别（0男 1女）
     */
    private Integer sex;
    /**
     * 班级id
     */
    private Long classInfoId;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 手机
     */
    private String tel;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 班级名称
     */
    @TableField(exist = false)
    private String className;


}
