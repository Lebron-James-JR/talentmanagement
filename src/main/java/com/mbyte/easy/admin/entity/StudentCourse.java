package com.mbyte.easy.admin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.mbyte.easy.common.entity.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生--课程关联表
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class StudentCourse extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 学生ID
     */
    private Long stuId;

    /**
     * 课程ID
     */
    private Integer courseId;

    /**
     * 分数
     */
    private Float score;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private String studentName;

    @TableField(exist = false)
    private String className;

    @TableField(exist = false)
    private String sex;


}
