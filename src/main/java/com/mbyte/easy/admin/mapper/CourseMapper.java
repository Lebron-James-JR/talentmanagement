package com.mbyte.easy.admin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程信息表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface CourseMapper extends BaseMapper<Course> {

}
