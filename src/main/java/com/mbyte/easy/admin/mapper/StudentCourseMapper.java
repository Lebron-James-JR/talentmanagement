package com.mbyte.easy.admin.mapper;

import com.mbyte.easy.admin.entity.StudentCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbyte.easy.admin.vo.CourseVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学生--课程关联表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface StudentCourseMapper extends BaseMapper<StudentCourse> {

    /**
     * 得到学生对应的课程列表
     * @param sid
     * @return
     */
    List<CourseVo> getCourseStu(@Param("sid") Long sid);

    /**
     * 得到指定课程下的学生名单以及成绩
     * @param cid
     * @return
     */
    List<StudentCourse> getStuList(@Param("cid") Long cid);
}
