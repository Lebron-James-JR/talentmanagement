package com.mbyte.easy.admin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.ClassInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mbyte.easy.admin.entity.Student;

import java.util.List;

/**
 * <p>
 * 班级信息表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface ClassInfoMapper extends BaseMapper<ClassInfo> {
    /**
     * 查询全部班级信息
     * @param ipage
     * @return
     */
    IPage<ClassInfo> selectAll(Page<ClassInfo> ipage);
}
