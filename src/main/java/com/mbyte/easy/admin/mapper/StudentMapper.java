package com.mbyte.easy.admin.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mbyte.easy.admin.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 学生信息表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2020-10-24
 */
public interface StudentMapper extends BaseMapper<Student> {

    int saveStu(@Param("student") Student student);

    int updateStu(@Param("student") Student student);

    /**
     * 查询全部学生
     * @param ipage
     * @return
     */
    IPage<Student> selectStu(Page<Student> ipage);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    int deleteStuById(@Param("id")Long id);

    /**
     * 得到学生信息
     * @param id
     * @return
     */
    Student getStuById(@Param("id") Long id);

    /**
     * 得到学生列表
     * @param cid  班级ID
     * @return
     */
    List<Student> getStu(@Param("cid") Long cid);
}
