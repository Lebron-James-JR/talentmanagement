package com.mbyte.easy.admin.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author zte
 * @since 2020/11/25 23:38
 */
@Data
@Accessors(chain = true)
public class CourseVo {
    /**
     * 课程ID
     */
    private Long id;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 选课时间
     */
    private LocalDateTime createTime;

    /**
     * 课程分数
     */
    private Float score;
}
